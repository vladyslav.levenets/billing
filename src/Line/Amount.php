<?php

namespace Billing\Line;

use Billing\Line\Monetary;
use Brick\Money\Money;
use Brick\Math\RoundingMode;

/**
 * This class describes an amount.
 */
class Amount
{
    private $_amount;

    /**
     * Constructs a new instance.
     *
     * @param float|self $amount The amount.
     */
    public function __construct($amount = 1)
    {
        if ($amount instanceof self) {
            $this->_amount = $amount->getAmount();
        } else {
            $this->_amount = floatval($amount);
        }
    }

    /**
     * Initializes the given amount.
     *
     * @param float|self $amount The amount.
     *
     * @return self   The Amount class.
     */
    public static function init($amount = 1): self
    {
        return new self($amount);
    }

    /**
     * Gets the amount.
     *
     * @return float|integer The amount.
     */
    public function getAmount(): float
    {
        if ($this->_amount) {
            return $this->_amount;
        } else {
            return 1;
        }
    }

    /**
     * Applies the given money.
     *
     * @param Money $money The money.
     *
     * @return Money The Money.
     */
    public function apply(Money $money): Money
    {
        return $money->multipliedBy($this->_amount, RoundingMode::HALF_EVEN);
    }
}