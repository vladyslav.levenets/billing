<?php

namespace Billing\Line;

use Billing\Line\Monetary;

use Brick\Money\Money;
use Brick\Math\RoundingMode;

/**
 * This class describes a discount.
 */
class Discount
{
    private $_discount;
    private $_discount_value;

    /**
     * Constructs a new instance.
     *
     * @param float|self|Monetary|Money|null $discount The discount.
     */
    public function __construct($discount = 0)
    {
        if ($discount instanceof self) {
            $discount = $discount->getValue();
        }

        if ($discount instanceof Monetary) {
            $this->_discount_value = $discount->getValue();
        } elseif ($discount instanceof Money) {
            $this->_discount_value = $discount;
        } else {
            $this->_discount = floatval($discount);
        }
    }

    /**
     * Initializes the given discount.
     *
     * @param float|self|Monetary|Money|null $discount The discount
     * 
     * @return self   The Discount class
     */
    public static function init($discount = 0): self
    {
        return new self($discount);
    }

    /**
     * Gets the value.
     *
     * @return Money|float|null The vat value.
     */
    public function getValue() {
        if ($this->_discount_value) {
            return $this->_discount_value;
        } else {
            return $this->_discount;
        }
    }
    
    /**
     * Gets the discount.
     *
     * @param Money|null $money The money.
     *
     * @return Money|null The discount.
     */
    public function getDiscount(?Money $money = null): ?Money
    {
        if ($this->_discount_value) {
            return $this->_discount_value;
        } elseif ($money) {
            return $money->multipliedBy(
                $this->_parseDiscount(),
                RoundingMode::HALF_EVEN
            );
        } else {
            return null;
        }
    }

    /**
     * Applies the given money.
     *
     * @param Money $money The money.
     *
     * @return Money The Money class.
     */
    public function apply(Money $money): Money
    {
        if ($this->_discount_value) {
            $discount = $this->_discount_value;
            if ($this->_discount_value > $money) {
                $discount = $money;
            }
            
            return $money->minus(
                $discount,
                RoundingMode::HALF_EVEN
            );
        } else {
            return $money->multipliedBy(
                $this->_parseValue(),
                RoundingMode::HALF_EVEN
            );
        }
    }


    /**
     * List money
     *
     * @param Money   $money The money.
     *
     * @return Money[] The Discount array.
     */
    public function list(Money $money): array
    {
        if ($this->_discount_value) {
            $discount = $this->_discount_value;
            if ($this->_discount_value > $money) {
                $discount = $money;
            }

            return [
                $money->minus(
                    $discount,
                    RoundingMode::HALF_EVEN
                ),
                $discount
            ];
        } else {
            return [
                $money->multipliedBy(
                    $this->_parseValue(),
                    RoundingMode::HALF_EVEN
                ),
                $money->multipliedBy(
                    $this->_parseDiscount(),
                    RoundingMode::HALF_EVEN
                )
            ];
        }
    }

    /**
     * Get discount percentage.
     *
     * @return float The discount percentage.
     */
    private function _parseDiscount(): float
    {
        if ($this->_discount > 100) {
            return 1;
        } elseif ($this->_discount) {
            return floatval($this->_discount) / 100;
        }

        return 0;
    }

    /**
     * Get value percentage after discount.
     *
     * @return float The value percentage after discount.
     */
    private function _parseValue(): float
    {
        if ($this->_discount > 100) {
            return 0;
        } elseif ($this->_discount) {
            return (100 - floatval($this->_discount)) / 100;
        }

        return 1;
    }
}