<?php

namespace Billing\Line;

use Billing\Line\Monetary;

use Brick\Money\Money;
use Brick\Math\RoundingMode;

/**
 * This class describes a vat.
 */
class Vat
{
    private $_vat;
    private $_vat_in_value;

    /**
     * Constructs a new instance.
     *
     * @param float|null  $vat          The vat.
     * @param boolean|null $vat_in_value The vat in value.
     */
    public function __construct($vat = 0, ?bool $vat_in_value = null)
    {
        if ($vat instanceof self) {
            $this->_vat = $vat->getValue();
            $this->_vat_in_value = $vat_in_value ?? $vat->getInValue();
        } else {
            $this->_vat = floatval($vat);
            $this->_vat_in_value = $vat_in_value ?? false;
        }
    }

    /**
     * Initializes the given vat.
     *
     * @param float|null   $vat          The vat.
     * @param boolean|null $vat_in_value The vat in value.
     *
     * @return self        The new Vat class.
     */
    public static function init($vat = 0, ?bool $vat_in_value = null): self
    {
        return new self($vat, $vat_in_value);
    }

    /**
     * Sets in value.
     *
     * @param boolean|null $vat_in_value The vat in value.
     *
     * @return self        The new Vat class.
     */
    public function setInValue(?bool $vat_in_value = false): self
    {
        return new self($this->_vat, $vat_in_value);
    }

    /**
     * Gets the vat.
     *
     * @param Money|null $money The money.
     *
     * @return Money|null The vat.
     */
    public function getVat(?Money $money = null): ?Money
    {
        if ($money) {
            return $money->multipliedBy(
                $this->_parseVat(),
                RoundingMode::HALF_EVEN
            );
        } else {
            return null;
        }
    }

    /**
     * Gets the value.
     *
     * @return float|null The vat value.
     */
    public function getValue(): ?float
    {
        return $this->_vat;
    }


    /**
     * Gets the value.
     *
     * @return bool|null The vat value.
     */
    public function getInValue(): ?bool
    {
        return $this->_vat_in_value;
    }

    /**
     * Applies the given money.
     *
     * @param Money|null $money The money.
     *
     * @return Money|null The Money after calc.
     */
    public function apply(?Money $money): ?Money
    {
        if (!$money) {
            return null;
        } elseif ($this->_vat_in_value) {
            return $money;
        } else {
            return $money->multipliedBy(
                $this->_parseValue(),
                RoundingMode::HALF_EVEN
            );
        }
    }

    /**
     * Removes the given money.
     *
     * @param Money|null $money The money.
     *
     * @return Money|null The Money after calc.
     */
    public function remove(?Money $money): ?Money
    {
        if (!$money) {
            return null;
        } elseif ($this->_vat_in_value) {
            return $money->multipliedBy(
                $this->_extractValue(),
                RoundingMode::HALF_EVEN
            );
        } else {
            return $money;
        }
    }

    /**
     * List money.
     *
     * @param Money $money The money.
     *
     * @return Money[] The list of Money.
     */
    public function list(Money $money): array
    {
        if ($this->_vat_in_value) {
            return [
                $money,
                $money->multipliedBy(
                    $this->_extractValue(),
                    RoundingMode::HALF_EVEN
                ),
                $money->multipliedBy(
                    $this->_extractVat(),
                    RoundingMode::HALF_EVEN
                ),
                $money
            ];
        } else {
            return [
                $money,
                $money,
                $money->multipliedBy(
                    $this->_parseVat(),
                    RoundingMode::HALF_EVEN
                ),
                $money->multipliedBy(
                    $this->_parseValue(),
                    RoundingMode::HALF_EVEN
                ),
            ];
        }
    }

    /**
     * Get vat percentage.
     *
     * @return float The vat percentage.
     */
    private function _parseVat(): float
    {
        $vat = $this->_vat? floatval($this->_vat): 0;
        return $vat / 100;
    }

    /**
     * Get value percentage after adding vat.
     *
     * @return float The value with vat percentage.
     */
    private function _parseValue(): float
    {
        $vat = $this->_vat? floatval($this->_vat): 0;
        return ($vat + 100) / 100;
    }

    /**
     * Get vat percentage in value.
     *
     * @return float The vat percentage.
     */
    private function _extractVat(): float
    {
        $vat = $this->_vat? floatval($this->_vat): 0;
        return $vat / (100 + $vat);
    }

    /**
     * Get value percentage after extracting vat.
     *
     * @return float The value percentage.
     */
    private function _extractValue(): float
    {
        $vat = $this->_vat? floatval($this->_vat): 0;
        return 100 / (100 + $vat);
    }
}