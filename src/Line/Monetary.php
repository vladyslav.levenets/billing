<?php

namespace Billing\Line;

use Brick\Money\Money;
use Brick\Math\RoundingMode;

/**
 * This class describes a monetary.
 */
class Monetary
{
    private $_money;
    private $_currency;

    /**
     * Constructs a new instance.
     *
     * @param Money|Monetary|float $money    The money.
     * @param string               $currency The currency.
     */
    public function __construct($money = 0, $currency = 'EUR')
    {
        $this->_currency = $currency;
        $this->_money = $this->_value($money);
    }

    /**
     * Initializes the given monetary.
     *
     * @param Money|Monetary|float $money    The money.
     * @param string               $currency The currency.
     *
     * @return self                The Monetary class.
     */
    public static function init($money = 0, $currency = 'EUR'): self
    {
        return new self($money, $currency);
    }

    /**
     * Gets the value.
     *
     * @return Money The value.
     */
    public function getValue(): Money
    {
        return $this->_money;
    }

    /**
     * Applies the given variable.
     *
     * @param Amount|Discount|Vat $variable The variable.
     *
     * @return self               The new Monetary class.
     */
    public function apply($variable): self
    {
        return $this->_new($variable->apply($this->_money));
    }

    /**
     * Removes the given variable.
     *
     * @param Amount|Discount|Vat $variable The variable.
     *
     * @return self               The new Monetary class.
     */
    public function remove($variable): self
    {
        return $this->_new($variable->remove($this->_money));
    }

    /**
     * List money.
     *
     * @param Discount|Vat $variable The variable.
     *
     * @return Monetary[]  The list of Monetary from Discount|Vat.
     */
    public function list($variable): array
    {
        $return_data = [];
        foreach ($variable->list($this->_money) as $value) {
            $return_data[] = $this->_new($value);
        }
        return $return_data;
    }

    /**
     * Sum of Money|Monetary|float.
     *
     * @param Money|Monetary|float $variable The variable.
     *
     * @return self          The new Monetary class.
     */
    public function plus($variable): self
    {
        return $this->_new(
            $this->_money->plus(
                $this->_value($variable),
                RoundingMode::HALF_EVEN
            )
        );
    }

    /**
     * Diference of Money|Monetary|float.
     *
     * @param Money|Monetary|float $variable The variable.
     *
     * @return self          The new Monetary class.
     */
    public function minus($variable): self
    {
        return $this->_new(
            $this->_money->minus(
                $this->_value($variable),
                RoundingMode::HALF_EVEN
            )
        );
    }

    /**
     * Product of Money|Monetary|float.
     *
     * @param Money|Monetary|float $variable The variable.
     *
     * @return self          The new Monetary class.
     */
    public function multipliedBy($variable): self
    {
        return $this->_new(
            $this->_money->multipliedBy(
                (string)$this->_value($variable)->getAmount(),
                RoundingMode::HALF_EVEN
            )
        );
    }

    /**
     * Result of division of Money|Monetary|float.
     *
     * @param Money|Monetary|float $variable The variable.
     *
     * @return self          The new Monetary class.
     */
    public function dividedBy($variable): self
    {
        return $this->_new(
            $this->_money->dividedBy(
                (string)$this->_value($variable)->getAmount(),
                RoundingMode::HALF_EVEN
            )
        );
    }

    /**
     * Formats this Money with the given NumberFormatter.
     *
     * @param string $formater The formater.
     *
     * @return string The formated string.
     */
    public function format($formater = 'pt_PT'): string
    {
        return $this->_money->formatTo($formater);
    }

    public function getAmount() {
        return (string) $this->_money->getAmount();
    }

    /**
     * Create new Monetary with curenc caracteristic.
     *
     * @param Money $money The money.
     *
     * @return self The new Monetary class.
     */
    private function _new(Money $money): self
    {
        return new self(
            (string)$money->getAmount(),
            $this->_currency
        );
    }

    /**
     * Convert Money|Monetary to Money.
     *
     * @param Money|Monetary $variable The variable.
     *
     * @return Money         The Money value.
     */
    private function _value($variable): Money
    {
        if ($variable instanceof self) {
            return $variable->getValue();
        } elseif ($variable instanceof Money) {
            return $variable;
        } else {
            return Money::of($variable, $this->_currency);
        }
    }
}