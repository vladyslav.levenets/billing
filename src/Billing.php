<?php

namespace Billing;

use Billing\Line;
use Billing\Line\Monetary;

/**
 * This class describes a billing.
 */
class Billing
{
    private $_counter = 0;
    private $_lines = [];
    private $_extra_discounts = [];

    private $_vat_list = [];
    private $_price;
    private $_discount;
    private $_illiquid_price;
    private $_extra_discount;
    private $_price_without_vat;
    private $_vat;
    private $_total;


    /**
     * Constructs a new instance.
     *
     * @param Line[]|Line|null $line The line.
     */
    public function __construct($line = null)
    {
        if (is_array($line)) {
            foreach ($line as $temp_line) {
                if ($temp_line instanceof Line) {
                    $this->addLine($temp_line);
                }
            }
        } elseif ($line instanceof Line) {
            $this->addLine($line);
        }
    }

    /**
     * Initializes the given line.
     *
     * @param Line[]|Line|null $line The line.
     *
     * @return self            The Billing class.
     */
    public static function init($line = null): self
    {
        return new self($line);
    }

    /**
     * Sets the extra discounts.
     *
     * @param array $extra_discounts The extra discounts.
     *
     * @return self The Billing class.
     */
    public function setExtraDiscounts($extra_discounts = []): self
    {
        if (is_array($extra_discounts)) {
            $this->_extra_discounts = $extra_discounts;
        } else {
            $this->_extra_discounts = [$extra_discounts];
        }

        $this->_parse();

        return $this;
    }

    /**
     * Gets the line.
     *
     * @param integer|null $id The identifier.
     *
     * @return array|Line|null      The line.
     */
    public function getLine(?int $id = null)
    {
        if ($id === null) {
            return $this->_lines;
        }

        return array_filter(
            $this->_lines,
            function ($line) use (&$id) {
                return $line->getId() == $id;
            }
        );
    }

    /**
     * Adds a line.
     *
     * @param Line $line The line.
     *
     * @return self The Billing class.
     */
    public function addLine(Line $line): self
    {
        $this->_lines[] = $line->setId(++$this->_counter);
        $this->_parse();

        return $this;
    }

    /**
     * Delete a line
     *
     * @param integer $id The identifier.
     *
     * @return self   The Billing class.
     */
    public function deleteLine(int $id): self
    {
        foreach ($this->_lines as $line_index => $line) {
            if ($line->getId() == $id) {
                unset($this->_lines[$line_index]);
            }
        }

        $this->_parse();

        return $this;
    }

    /**
     * Initializes the object variables.
     *
     * @return self The Billing class
     */
    private function _init(): self
    {
        $this->_vat_list = [];
        $this->_price = Monetary::init(0);
        $this->_discount = Monetary::init(0);
        $this->_illiquid_price = Monetary::init(0);
        $this->_extra_discount = Monetary::init(0);
        $this->_price_without_vat = Monetary::init(0);
        $this->_vat = Monetary::init(0);
        $this->_total = Monetary::init(0);

        return $this;
    }


    /**
     * Made calculations.
     *
     * @return self The Billing class.
     */
    private function _parse(): self
    {
        $this->_init();

        foreach ($this->_lines as $line) {
            $line->setExtraDiscounts($this->_extra_discounts);

            $this->_addVatList($line);

            $this->_price
                = $this->_price
                ->plus($line->getPrice());
            $this->_discount
                = $this->_discount
                ->plus($line->getDiscount());
            $this->_illiquid_price
                = $this->_illiquid_price
                ->plus($line->getIlliquidPrice());
            $this->_extra_discount
                = $this->_extra_discount
                ->plus($line->getExtraDiscount());
            $this->_price_without_vat
                = $this->_price_without_vat
                ->plus($line->getPriceWithoutVat());
            $this->_vat
                = $this->_vat
                ->plus($line->getVat());
            $this->_total
                = $this->_total
                ->plus($line->getTotal());
        }

        return $this;
    }

    /**
     * Adds a vat list.
     *
     * @param Line $line The line.
     *
     * @return self The Billing class.
     */
    private function _addVatList(Line $line): self
    {
        foreach (['total', $line->getVatInstance()->getValue()] as $type) {
            if ($this->_vat_list[$type] ?? false) {
                $this->_vat_list[$type]['price']
                    = $this->_vat_list[$type]['price']
                    ->plus($line->getPriceWithoutVat());
                $this->_vat_list[$type]['vat']
                    = $this->_vat_list[$type]['vat']
                    ->plus($line->getVat());
                $this->_vat_list[$type]['total']
                    = $this->_vat_list[$type]['total']
                    ->plus($line->getTotal());
            } else {
                $this->_vat_list[$type] = [
                    'price' => $line->getPriceWithoutVat(),
                    'vat' => $line->getVat(),
                    'total' => $line->getTotal(),
                ];
            }
        }

        ksort($this->_vat_list, SORT_STRING);

        return $this;
    }

    /**
     * Gets the vat list.
     *
     * @return array The vat list.
     */
    public function getVatList(): array
    {
        return $this->_vat_list;
    }

    /**
     * Gets the price.
     *
     * @return Monetary|null The price.
     */
    public function getPrice(): ?Monetary
    {
        return $this->_price;
    }

    /**
     * Gets the discount.
     *
     * @return Monetary|null The discount.
     */
    public function getDiscount(): ?Monetary
    {
        return $this->_discount;
    }

    /**
     * Gets the illiquid price.
     *
     * @return Monetary|null The extra discount.
     */
    public function getIlliquidPrice(): ?Monetary
    {
        return $this->_illiquid_price;
    }

    /**
     * Gets the extra discount.
     *
     * @return Monetary|null The extra discount.
     */
    public function getExtraDiscount(): ?Monetary
    {
        return $this->_extra_discount;
    }

    /**
     * Gets the price without vat.
     *
     * @return Monetary|null The vat.
     */
    public function getPriceWithoutVat(): ?Monetary
    {
        return $this->_price_without_vat;
    }

    /**
     * Gets the vat.
     *
     * @return Monetary|null The vat.
     */
    public function getVat(): ?Monetary
    {
        return $this->_vat;
    }

    /**
     * Gets the total.
     *
     * @return Monetary|null The total.
     */
    public function getTotal(): ?Monetary
    {
        return $this->_total;
    }
}