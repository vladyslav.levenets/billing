<?php

namespace Billing;

use Billing\Line\Amount;
use Billing\Line\Discount;
use Billing\Line\Monetary;
use Billing\Line\Vat;

/**
 * This class describes a line.
 */
class Line
{
    private $_id;
    private $_data;
    private $_extra_discounts = [];

    private $_unit_price;
    private $_price;
    private $_amount;
    private $_discount;
    private $_illiquid_price;
    private $_extra_discount;
    private $_price_without_vat;
    private $_vat;
    private $_total;

    /**
     * Constructs a new instance.
     *
     * array[price]    Monetary            The Monetary class
     *      [amount]   Amount              The Amount class
     *      [discount] Discount|Discount[] The Discount or array of Discount classes
     *      [vat]      Vat                 The Vat class
     *      [...]      ?any                Other variables in array
     *
     * @param array|null $data The data.
     */
    public function __construct(?array $data = [])
    {
        $this->_data = $data;

        $this->_parse();
    }


    /**
     * Initializes the given data.
     *
     * @param array|null $data The data.
     *
     * @return self      The Line class.
     */
    public static function init(?array $data = []): self
    {
        return new self($data);
    }


    /**
     * Updates the given data.
     *
     * @param array|null $data The data.
     *
     * @return self      The Line class.
     */
    public function update(?array $data = []): self
    {
        $this->_data = $data;

        $this->_parse();

        return $this;
    }

    private function _init(): self
    {
        $this->_price = Monetary::init(0);
        $this->_amount = Amount::init(1);
        $this->_discount = Monetary::init(0);
        $this->_illiquid_price = Monetary::init(0);
        $this->_extra_discount = Monetary::init(0);
        $this->_price_without_vat = Monetary::init(0);
        $this->_vat = Monetary::init(0);
        $this->_total = Monetary::init(0);

        return $this;
    }

    /**
     * Made calculations.
     *
     * @return self The Line class.
     */
    private function _parse()
    {
        $this->_init();

        $this->_unit_price = $this->_parsePrice();
        $vat = $this->_parseVat();
        // $price = $price->apply($vat);
        // $vat = $vat->setInValue(true);
        $this->_amount = $this->_parseAmouunt();
        $this->_price = $this->_unit_price->apply($this->_amount);

        foreach ($this->_parseDiscount() as $discount) {
            list($temp_price, $temp_discount) = $this->_price->list($discount);
            $this->_total = $this->_price = $temp_price;

            $this->_discount = $this->_discount->plus($temp_discount);
        }
        $this->_illiquid_price = $this->_price;
        foreach ($this->_parseExtraDiscount() as $extra_discount) {
            list($temp_price, $temp_discount) = $this->_price->list($extra_discount);
            $this->_total = $this->_price = $temp_price;

            $this->_extra_discount = $this->_extra_discount->plus($temp_discount);
        }

        list(
            $price_temp,
            $price_without_vat_temp,
            $vat_temp,
            $total_temp
        ) = $this->_price->list($vat);

        $this->_price = $price_temp;
        $this->_price_without_vat = $price_without_vat_temp;
        $this->_vat = $vat_temp;
        $this->_total = $total_temp;

        return $this;
    }

    private function _parsePrice(): Monetary
    {
        return Monetary::init($this->_data['price'] ?? 0);
    }

    private function _parseAmouunt(): Amount
    {
        return Amount::init($this->_data['amount'] ?? 1);
    }

    private function _parseVat(): Vat
    {
        return Vat::init($this->_data['vat'] ?? 0);
    }

    private function _parseDiscount(): array
    {
        if (!($this->_data['discount'] ?? false)) {
            return [];
        }

        $temp = is_array($this->_data['discount'])?
            $this->_data['discount']:
            [$this->_data['discount']];

        $discounts = [];
        foreach ($temp as $t) {
            $discounts[] = Discount::init($t);
        }

        return $discounts;
    }

    private function _parseExtraDiscount(): array
    {
        if (!($this->_extra_discounts ?? false)) {
            return [];
        }

        $temp = is_array($this->_extra_discounts)?
            $this->_extra_discounts:
            [$this->_extra_discounts];

        $discounts = [];
        foreach ($temp as $t) {
            $discounts[] = Discount::init($t);
        }

        return $discounts;
    }

    /**
     * Gets the data.
     *
     * @return array The data.
     */
    public function getData(): array
    {
        return $this->_data;
    }

    /**
     * Sets the extra discounts.
     *
     * @param array $extra_discounts The extra discounts.
     *
     * @return self The Line class.
     */
    public function setExtraDiscounts($extra_discounts = []): self
    {
        if (is_array($extra_discounts)) {
            $this->_extra_discounts = $extra_discounts;
        } else {
            $this->_extra_discounts = [$extra_discounts];
        }

        $this->_parse();

        return $this;
    }


    /**
     * Gets the identifier.
     *
     * @return integer The identifier.
     */
    public function getId(): int
    {
        return $this->_id;
    }

    /**
     * Sets the identifier.
     *
     * @param integer $id The new value.
     *
     * @return self   The Line class.
     */
    public function setId(int $id): self
    {
        $this->_id = $id;

        return $this;
    }

    /**
     * Gets the unit price.
     *
     * @return Monetary|null The price.
     */
    public function getUnitPrice(): ?Monetary
    {
        return $this->_unit_price;
    }

    /**
     * Gets the price.
     *
     * @return Monetary|null The price.
     */
    public function getPrice(): ?Monetary
    {
        return $this->_price;
    }

    /**
     * Gets the amount.
     *
     * @return Amount|null The amount.
     */
    public function getAmount(): ?Amount
    {
        return $this->_amount;
    }

    /**
     * Gets the discount.
     *
     * @return Monetary|null The discount.
     */
    public function getDiscount(): ?Monetary
    {
        return $this->_discount;
    }

    /**
     * Gets the illiquid price.
     *
     * @return Monetary|null The extra discount.
     */
    public function getIlliquidPrice(): ?Monetary
    {
        return $this->_illiquid_price;
    }

    /**
     * Gets the extra discount.
     *
     * @return Monetary|null The extra discount.
     */
    public function getExtraDiscount(): ?Monetary
    {
        return $this->_extra_discount;
    }

    /**
     * Gets the price without vat.
     *
     * @return Monetary|null The vat.
     */
    public function getPriceWithoutVat(): ?Monetary
    {
        return $this->_price_without_vat;
    }

    /**
     * Gets the vat.
     *
     * @return Monetary|null The vat.
     */
    public function getVat(): ?Monetary
    {
        return $this->_vat;
    }

    /**
     * Gets the vat instance.
     *
     * @return Vat|null The vat value.
     */
    public function getVatInstance(): ?Vat
    {
        return $this->_parseVat();;
    }

    /**
     * Gets the total.
     *
     * @return Monetary|null The total.
     */
    public function getTotal(): ?Monetary
    {
        return $this->_total;
    }

    /**
     * Gets the specified key.
     *
     * @param string $key The key.
     *
     * @return any   The variable in data.
     */
    public function get(string $key)
    {
        return $this->_data[$key] ?? null;
    }

    /**
     * Set the specified key.
     *
     * @param string $key   The key
     * @param any    $value The value
     *
     * @return self  The Line class.
     */
    public function set(string $key, $value): self
    {
        $this->_data[$key] = $value;

        $this->_parse();

        return $this;
    }
}