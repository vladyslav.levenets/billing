# Billing\Billing

A billing for PHP.

<!-- ## Introduction -->


### Installation

The library [vladyslav.levenets/billing](https://packagist.org/packages/vladyslav.levenets/billing) is installable via [Composer](https://getcomposer.org/):

```bash
composer require vladyslav.levenets/billing
```

### Requirements

This library requires PHP 7.1 or later.
This library requires brick/money 0.5.0 or later.

## Monetary

To create `Monetary`, call the `init()` factory method or initialize as `new Monetary()`:

```php
use Billing\Line\Monetary;

$money = Monetary::init(/*\Billing\Line\Monetary|\Brick\Money\Money|float*/10/*, 'EUR'*/);

$money->getValue(); // \Brick\Money\Money

$money->format(/*pt_PT*/); // (string)10.00 €

$money->plus(/*\Billing\Line\Monetary|\Brick\Money\Money|float*/$money); // new \Billing\Line\Monetary
$money->minus(/*\Billing\Line\Monetary|\Brick\Money\Money|float*/$money); // new \Billing\Line\Monetary
$money->multipliedBy(/*\Billing\Line\Monetary|\Brick\Money\Money|float*/$money); // new \Billing\Line\Monetary
$money->dividedBy(/*\Billing\Line\Monetary|\Brick\Money\Money|float*/$money); // new \Billing\Line\Monetary

$money->apply(/*\Billing\Line\Amount|\Billing\Line\Discount|\Billing\Line\Vat*/$var); // new \Billing\Line\Monetary
$money->list(/*\Billing\Line\Discount|\Billing\Line\Vat*/$var);  // new \Billing\Line\Monetary[]
```

## Amount

To create `Amount`, call the `init()` factory method or initialize as `new Amount()`:

```php
use Billing\Line\Amount;

$amount = Amount::init(/*\Billing\Line\Amount|float*/10);

$amount->getAmount(); // (float)10

$amount->apply(/*\Brick\Money\Money*/$money); // new \Brick\Money\Money
```

## Discount

To create `Discount`, call the `init()` factory method or initialize as `new Discount()`:

```php
use Billing\Line\Discount;

$discount = Discount::init(/*\Billing\Line\Discount|\Billing\Line\Monetary|\Brick\Money\Money|float*/50);

$discount->getDiscount(/*\Brick\Money\Money|null*/$money); // \Brick\Money\Money|null
$discount->getValue() // \Brick\Money\Money|float|null

$discount->apply(/*\Brick\Money\Money*/$money); // new \Brick\Money\Money
$discount->list(/*\Brick\Money\Money*/$money); // new \Brick\Money\Money[]
```

## Vat

To create `Vat`, call the `init()` factory method or initialize as `new Vat()`:

```php
use Billing\Line\Vat;

$vat = Vat::init(/*\Billing\Line\Vat|float*/10/*, false*/);

$vat->setInValue(/*false*/); // new \Billing\Line\Vat
$vat->getVat(/*\Brick\Money\Money|null*/$money) // new \Brick\Money\Money|null
$vat->getValue() // float|null
$vat->getInValue() // bool|null

$vat->apply(/*\Brick\Money\Money*/$money); // new \Brick\Money\Money
$vat->list(/*\Brick\Money\Money*/$money); // new \Brick\Money\Money[]
```

## Line

To create `Line`, call the `init()` factory method or initialize as `new Line()`:

```php
use Billing\Line;

$data = [
    'price' => $price,
    'amount' => /*\Billing\Line\Amount|float|null*/$amount,
    'discount' => /*\Billing\Line\Discount|float|null*/$discount // or [$discount1, $discount2],
    'vat' => /*\Billing\Line\Vat|float|null*/$vat,
    //... => ...
];

$line = Line::init($data);

$line->update($data); // \Billing\Line
$line->getData() // $data

$line->setExtraDiscounts(/*\Billing\Line\Discount|float|null*/$discount/* or [$discount1, $discount2]*/);  // \Billing\Line

$line->getUnitPrice() // \Billing\Line\Monetary
$line->getPrice() // \Billing\Line\Monetary
$line->getAmount() // \Billing\Line\Amount
$line->getDiscount() // \Billing\Line\Monetary
$line->getIlliquidPrice() // \Billing\Line\Monetary
$line->getExtraDiscount() // \Billing\Line\Monetary
$line->getPriceWithoutVat() // \Billing\Line\Monetary
$line->getVat() // \Billing\Line\Monetary
$line->getTotal() // \Billing\Line\Monetary
```

## Billing

To create `Billing`, call the `init()` factory method or initialize as `new Billing()`:

```php
use Billing\Billing;

$billing = Billing::init(/*\Billing\Line|null*/$line/* or [$line1, $line2]*/);

$billing->setExtraDiscounts($discount/* or [$discount1, $discount2]*/); // \Billing\Billing
$billing->getLine(/*int|null*/$id); // \Billing\Line[]|\Billing\Line|null
$billing->addLine(/*\Billing\Line*/$line); // \Billing\Billing
$billing->deleteLine(); // \Billing\Billing

$billing->getVatList(); // array(vat_percent => [price(\Billing\Line\Monetary), vat(\Billing\Line\Monetary), total(\Billing\Line\Monetary)])
$billing->getPrice() // \Billing\Line\Monetary
$billing->getDiscount() // \Billing\Line\Monetary
$billing->getIlliquidPrice() // \Billing\Line\Monetary
$billing->getExtraDiscount() // \Billing\Line\Monetary
$billing->getPriceWithoutVat() // \Billing\Line\Monetary
$billing->getVat() // \Billing\Line\Monetary
$billing->getTotal() // \Billing\Line\Monetary
```
