<?php
require_once __DIR__ . '/../vendor/autoload.php'; // Autoload files using Composer autoload

use Billing\Billing;
use Billing\Line;
use Billing\Line\Amount;
use Billing\Line\Discount;
use Billing\Line\Monetary;
use Billing\Line\Vat;

$price = Monetary::init(10);

$amount = Amount::init(2);

$discount = Discount::init(10);
$discount_2 = Discount::init(Monetary::init('18'));

$vat = Vat::init(23);

$price = $price->apply($vat);

$vat = $vat->setInValue(true);

$line = Line::init([
    'price' => $price,
    'amount' => $amount,
    'discount' => [
        $discount,
        // $discount_2,
    ],
    'vat' => $vat,
]);

$billing = Billing::init($line);

$billing->setExtraDiscounts(Discount::init(50));

echo "\n
***Result***\n
price: {$billing->getPrice()->format()}\n
discount: {$billing->getDiscount()->format()}\n
extra_discount: {$billing->getExtraDiscount()->format()}\n
price without vat: {$billing->getPriceWithoutVat()->format()}\n
vat: {$billing->getVat()->format()}\n
total: {$billing->getTotal()->format()}\n";

echo "\n
***Vat list***\n";
foreach ($billing->getVatList() as $vat_list_key => $vat_list) {
echo "\n
Tax: {$vat_list_key}\n
price: {$vat_list['price']->format()}\n
vat: {$vat_list['vat']->format()}\n
total: {$vat_list['total']->format()}\n";
}